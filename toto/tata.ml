type foobar =
  | Foo of int
  | Bar of string * bool
  | Baz

and foobar' = {
  foo' : int list [@default [ 42 ]] ;
  bar' : string * bool ;
}

[@@deriving ez]

