let test_toto () =
  let open Toto in
  assert (get_foo_exn (Foo 42) = 42) ;
  assert (get_bar_exn (Bar "tozzzz") = "tozzzz") ;
  assert (is_foo @@ Foo 42) ;
  assert (not @@ is_bar @@ Foo 23) ;
  assert (None = get_bar_opt (Foo 144)) ;
  assert (Some "lel" = get_bar_opt (Bar "lel")) ;
  Format.printf "Toto pass!\n"

let test_tata () =
  let open Tata in
  assert (Foo 42 = Foo 42) ;
  Format.printf "Tata pass!\n"

let () =
  test_toto () ;
  test_tata () ;
  ()
